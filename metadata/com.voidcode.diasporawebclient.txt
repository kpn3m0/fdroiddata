Categories:Internet
License:GPLv3
Web Site:https://github.com/voidcode/Diaspora-Webclient
Source Code:https://github.com/voidcode/Diaspora-Webclient
Issue Tracker:https://github.com/voidcode/Diaspora-Webclient/issues

Auto Name:Diaspora Webclient
Summary:Diaspora Social Network Client
Description:
Client for the Diaspora social network.
.

Repo Type:git
Repo:https://github.com/voidcode/Diaspora-Webclient.git

Build:1.3,3
    commit=26d7120fea1af5835a17537bebeef6df523d57e6
    target=android-10

Build:1.5,6
    commit=a153d5f996f284da44d5defa25ce601fd76b53ad
    target=android-10
    prebuild=mkdir libs && \
        mv google-api-translate-java-0.97.jar libs/ && \
        mv microsoft-translator-java-api-0.4-jar-with-dependencies.jar libs/

Build:1.6.3,10
    commit=8673ac9367dce08fecfde0f4858a03df4647864a
    target=android-10
    prebuild=mkdir libs && \
        mv google-api-translate-java-0.97.jar libs/ && \
        mv microsoft-translator-java-api-0.4-jar-with-dependencies.jar libs/

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.6.3
Current Version Code:10

