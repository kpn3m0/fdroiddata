Categories:Internet
License:Apache2
Web Site:https://code.google.com/p/connectbot
Source Code:https://code.google.com/p/connectbot/source
Issue Tracker:https://code.google.com/p/connectbot/issues

Auto Name:ConnectBot
Summary:SSH and local shell client
Description:
An SSH and telnet client, and terminal emulator for local shell.
Supports multiple sessions, including running them concurrently.

[[org.pocketworkstation.pckeyboard]],
[[com.anysoftkeyboard.languagepack.SSH]] and the Android
PC-keyboard-layout in Android 4.1 are well suited to terminal work.
.

Repo Type:git
Repo:https://code.google.com/p/connectbot.git

Build:1.7.1,323
    disable=from 2010, doesn't build since NDK r8
    commit=v1.7.1

Build:22-46-35-snapshot,365
    commit=716cdaa48
    forceversion=yes
    prebuild=sed -i 's/minSdkVersion=\"3\"/minSdkVersion=\"4\"/g' AndroidManifest.xml
    buildjni=yes

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.7.1
Current Version Code:365

