Categories:Office
License:GPLv3
Web Site:https://launchpad.net/pushupbuddy
Source Code:https://code.launchpad.net/~portstrom/pushupbuddy/trunk
Issue Tracker:https://bugs.launchpad.net/pushupbuddy

Auto Name:Pushup Buddy
Summary:Count push-ups using sensors
Description:
Uses the proximity sensor of your phone to log your push-up workout.
.

Repo Type:bzr
Repo:lp:~portstrom/pushupbuddy/trunk

Build:2014-01-08,20140108
    disable=upstream needs fixes
    commit=fredrik_portstrm-20140529160407-8hl5h67uaxbz04mc

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:2014-01-08
Current Version Code:20140108

