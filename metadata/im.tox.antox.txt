Categories:Internet
License:GPLv3
Web Site:http://tox.im
Source Code:https://github.com/Astonex/Antox
Issue Tracker:https://github.com/Astonex/Antox/issues

Auto Name:Antox
Summary:Secure and easy to use messenging
Description:
Tox aims to be an easy to use, all-in-one communication platform that ensures
their users full privacy and secure message delivery.

Every peer is represented as a byte string (the public key [Tox ID] of the
peer). By using torrent-style DHT, peers can find the IP of other peers by
using their Tox ID. Once the IP is obtained, peers can initiate a secure
(leveraging the NaCl library) connection with each other. Once the connection
is made, peers can exchange messages, send files, start video chats, etc.
using encrypted communications.
.

Repo Type:git
Repo:https://github.com/Astonex/Antox.git

Build:0.6.6,12
    disable=see notes
    commit=05c9eff2fe3e89b1c6468586b2bb8cd2c4f10d0e
    subdir=app
    gradle=yes

Maintainer Notes:
TODO: extlibs=jsoup/jsoup-1.7.3.jar
TODO: Cannot verify wearable-preview-support.jar, see http://developer.android.com/wear/preview/start.html
TODO: Cannot build jni libs (app/src/main/jniLibs/armeabi)
.

Auto Update Mode:None
Update Check Mode:Static
Current Version:0.6.6
Current Version Code:12

